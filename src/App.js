import React from "react";
import { Route, Switch } from "react-router-dom";
import NavBar from "./components/navbar/NavBar";
import Example from "./pages/Example";
import GetInsurance from "./pages/GetInsurance";
import HomePage from "./pages/HomePage";
import Insurance2 from './pages/Insurance2'

class App extends React.Component {

  
  render() {
    return (
      <div>
        <NavBar />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/getinsurance" component={GetInsurance} />
          <Route exact path="/insurance2" component={Insurance2} />
          <Route exact path="/example" component={Example} />
        </Switch>
      </div>
    );
  }
}
export default App;
