import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";
import { InputLabel } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },

  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
  "Self",
  "Mother",
  "Father",
  "Spouse",
  "Mother-in-Law",
  "Father-in-Law",
  "Son",
  "Daughter",
];

export default function HealthTab() {
  const classes = useStyles();
  // const theme = useTheme();
  const [personName, setPersonName] = React.useState([]);

  const handleChange = (event) => {
    setPersonName(event.target.value);
  };

  return (
    <div class="panel panel-default">
      <div class="panel-body">
        <form>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <input
                  type="text"
                  name="first_name"
                  id="first_name"
                  class="form-control input-sm"
                  placeholder="pincode"
                />
              </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <FormControl className={classes.formControl}>
                  <InputLabel
                    style={{ backgroundColor: "white" }}
                    id="demo-mutiple-checkbox-label"
                  >
                    Add Members
                  </InputLabel>
                  <Select
                    labelId="demo-mutiple-checkbox-label"
                    id="demo-mutiple-checkbox"
                    multiple
                    value={personName}
                    onChange={handleChange}
                    input={<Input />}
                    renderValue={(selected) => selected.join(", ")}
                    MenuProps={MenuProps}
                  >
                    {names.map((name) => (
                      <MenuItem key={name} value={name}>
                        <Checkbox checked={personName.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
            </div>
          </div>

          <div class="form-group">
            <input
              type="email"
              name="email"
              id="text"
              class="form-control input-sm"
              placeholder="Mobile number: "
            />
          </div>

          <input
            type="submit"
            value="Register"
            class="btn btn-info btn-block"
          />
        </form>
      </div>
    </div>
  );
}
