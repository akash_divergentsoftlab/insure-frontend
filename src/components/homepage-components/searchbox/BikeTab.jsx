import React from "react";
import { Link } from "react-router-dom";

export default function BikeTab() {
  return (
    <div class="panel panel-default">
      <div class="panel-body">
        <form>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <input
                  type="text"
                  name="first_name"
                  id="first_name"
                  class="form-control input-sm"
                  placeholder="enter city: "
                />
              </div>
            </div>
          </div>

          <Link to="/Insurance2">
            <button type="submit" class="btn btn-info btn-block">
              View Bike Insurance
            </button>
          </Link>
        </form>
      </div>
    </div>
  );
}
