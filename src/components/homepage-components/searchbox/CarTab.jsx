import React from "react";

export default function CarTab() {
  return (
    <div class="panel panel-default">
      <div class="panel-body">
        <form>
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <input
                  type="text"
                  name="first_name"
                  id="first_name"
                  class="form-control input-sm"
                  placeholder="enter city: "
                />
              </div>
            </div>
          </div>

          <div class="form-group">
            <input
              type="email"
              name="email"
              id="text"
              class="form-control input-sm"
              placeholder="Mobile number: "
            />
          </div>

          <input
            type="submit"
            value="Register"
            class="btn btn-info btn-block"
          />
        </form>
      </div>
    </div>
  );
}
