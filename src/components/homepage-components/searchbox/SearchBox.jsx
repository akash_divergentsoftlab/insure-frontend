import React from "react";
import CarTab from "./CarTab";
import HealthTab from "./HealthTab";
import BikeTab from "./BikeTab";
import "./searchbox.scss";

export default function SearchBox() {
  return (
    <div className="searchbox">
      <div class="tabs">
        <input type="radio" name="tabs" id="tabone" checked="checked" />
        <label for="tabone">Health</label>
        <div class="tab">
          <h1>Bike Insurance</h1>
          <hr />
          <HealthTab />
        </div>

        <input type="radio" name="tabs" id="tabtwo" />
        <label for="tabtwo">Car</label>
        <div class="tab">
          <h1>Car Insurance</h1>
          <CarTab/>
        </div>

        <input type="radio" name="tabs" id="tabthree" />
        <label for="tabthree">Bike</label>
        <div class="tab">
          <h1>Third tab content</h1>
          <BikeTab/>
        </div>
      </div>
    </div>
  );
}
