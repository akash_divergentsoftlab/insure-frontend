/* eslint-disable no-use-before-define */
import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete"; 

export default function WhenRegisterAutocomplete() {
  return (
    <Autocomplete
      id="combo-box-demo"
      onChange={(event, value) => console.log(value)}
      options={topBikeVariants}
      getOptionLabel={(option) => option.makeYear}
      style={{ width: 300 }}
      renderInput={(params) => (
        <TextField {...params} label="Regitration year" variant="outlined" />
      )}
    />
  );
}

// Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
const topBikeVariants = [
  { makeYear: "2021" },
  { makeYear: "2020" },
  { makeYear: "2019" },
  { makeYear: "2018" },
  { makeYear: "2017" },
  { makeYear: "2016" },
  { makeYear: "2015" },
  { makeYear: "2014" },

];
