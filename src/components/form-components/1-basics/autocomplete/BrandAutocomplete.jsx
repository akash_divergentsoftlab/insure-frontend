/* eslint-disable no-use-before-define */
import React, { useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import axios from "axios";
import { URl } from "../../../../BASICURL";

export default function BrandAutocomplete() {
  const [brands, setBrands] = useState([]);

  const getBrands = () => {
    axios.get(`${URl}/bikebrands/`).then((response) => {
      //   console.log(response);
      const myBrand = response.data;
      setBrands(myBrand);
      //   console.log(myBrand)
    });
  };

  useEffect(() => getBrands(), []);

  return (
    <Autocomplete
      id="combo-box-demo"
      options={brands}
      onChange={(event, value) => console.log(value)}
      getOptionLabel={(option) => option.brandName}
      style={{ width: 300 }}
      renderInput={(params) => (
        <TextField {...params} label="Bike Brand" variant="outlined" required />
      )}
    />
  );
}

// Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
