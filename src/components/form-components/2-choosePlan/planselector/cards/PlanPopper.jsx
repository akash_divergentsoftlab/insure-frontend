import React from 'react'

export default function PlanPopper() {
    return (
        <div>
            <h4>Comprehensive Cover</h4>
            <p>(Covers own damage + damage to others)</p>
            <small>Starting from (Excl. GST)</small>
            <p><b>₹1,140</b></p>
        </div>
    )
}
