import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Popper from "@material-ui/core/Popper";
import Fade from "@material-ui/core/Fade";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import ErrorIcon from "@material-ui/icons/Error";
import Typography from "@material-ui/core/Typography";
import PlanPopper from "./PlanPopper";
import { URl } from "../../../../../BASICURL";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    border: "1px solid",
    padding: theme.spacing(1),
    backgroundColor: theme.palette.background.paper,
  },

  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
}));

export default function PlanCard() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [plancard, setPlancard] = useState([]);

  const getPlanCards = () => fetch(`${URl}/bikeplan/`).then((res) => res.json());

  useEffect(() => {
    getPlanCards().then((plancard) => setPlancard(plancard));
    console.log(plancard)
  }, []);

  const handleClick = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "transitions-popper" : undefined;

  return (
    <>
      {plancard.map((item) => (
        <div className="card-div" style={{ width: "300px" }} >
          <Link to={item.planType}>
          <Card className={classes.root} key={item.planId} variant="outlined">
            <CardContent>
              <Typography variant="h5" component="h2">
                {item.planType}
              </Typography>
              <Typography className={classes.pos} color="textSecondary">
                <ErrorIcon
                  aria-describedby={id}
                  type="button"
                  onClick={handleClick}
                />
                <Popper id={id} open={open} anchorEl={anchorEl} transition>
                  {({ TransitionProps }) => (
                    <Fade {...TransitionProps} timeout={350}>
                      <div className={classes.paper}>
                        <PlanPopper />
                      </div>
                    </Fade>
                  )}
                </Popper>
              </Typography>
              <Typography variant="body2" component="p">
                {item.description}
              </Typography>
            </CardContent>
            <CardActions>
              <h1>₹{item.planAmount}</h1>
            </CardActions>
          </Card>
          </Link>
      </div>
      ))}
    </>
  );
}
