import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
  
    border: "1px solid black",
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function ChooseDurationCard() {
  const classes = useStyles();

  return (
    <div className="chooseduration">
      <Card className={classes.root}>
        <CardContent>
          <Typography variant="h5" component="h2">
            1 YEAR
          </Typography>
          <Typography className={classes.pos} color="textSecondary">
            Premium
          </Typography>
          <Typography variant="body2" component="p">
            ₹702
            <br />
            <br />
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}
