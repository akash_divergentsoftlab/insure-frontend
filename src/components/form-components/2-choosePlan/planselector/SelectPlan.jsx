import React from "react";
import PlanCard from "./cards/PlanCard";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  control: {
    padding: theme.spacing(2),
  },
}));

export default function SelectPlan() {
  // eslint-disable-next-line
  const [spacing, setSpacing] = React.useState(2);
  const classes = useStyles();

  return (
    <div>
      <Grid container className={classes.root} spacing={2}>
        <Grid item xs={12}>
          <Grid container justifyContent="center" spacing={spacing}>
            <Grid item>
              <PlanCard />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <br />
    </div>
  );
}
