import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ChooseDurationCard from "./cards/ChooseDurationCard";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  control: {
    padding: theme.spacing(2),
  },
}));

export default function SpacingGrid() {
  // eslint-disable-next-line
  const [spacing, setSpacing] = React.useState(2);
  const classes = useStyles();



  return (
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <Grid container justifyContent="center" spacing={spacing}>
          {[0].map((value) => (
            <Grid key={value} item>
              <ChooseDurationCard />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  );
}
