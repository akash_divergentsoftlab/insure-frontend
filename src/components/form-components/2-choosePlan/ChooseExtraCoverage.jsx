import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import { FormControlLabel } from "@material-ui/core";

class ChooseExtraCoverage extends React.Component {
  state = {
    checkedA: false,
    checkedB: false,
    checkedC: false,
  };

  handleChange = (name) => (event) => {
    this.setState({ [name]: event.target.checked });
  };

  render() {
    return (
      <div>
        <FormControlLabel
          control={
            <Checkbox
              checked={this.state.checkedA}
              onChange={this.handleChange('checkedA')}
              value="checkedA"
              color="primary"
            />
          }
          label="Personal Accident Cover of Rs.15 Lakhs (valid for 1 year) - ₹220"
        />
        <br/>
        <FormControlLabel
          control={
            <Checkbox
              checked={this.state.checkedB}
              onChange={this.handleChange('checkedB')}
              value="checkedB"
              color="primary"
            />
          }
          label="Passenger Cover - ₹420"
        />
        <br/>
        <FormControlLabel
          control={
            <Checkbox
              checked={this.state.checkedC}
              onChange={this.handleChange('checkedC')}
              value="checkedC"
              color="primary"
            />
          }
          label="PA Cover for paid driver - IMT 17 - ₹210"
        />
      </div>
    );
  }
}

export default ChooseExtraCoverage;
