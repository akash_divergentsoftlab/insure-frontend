import React from "react";
import "./userdetailform.css";

function UserDetail() {
  return (
    <div className="userdetailform">
      <div className="container">
        <form id="contact" action="" method="post">
          <h3>Owner Details</h3>
          <h4>Contact us today, and get reply with in 24 hours!</h4>
          <fieldset>
            <input
              placeholder="Full name"
              type="text"
              tabindex="1"
              required
              autofocus
            />
          </fieldset>
          <fieldset>
            <input
              placeholder="pincode"
              type="text"
              tabindex="1"
              required
              autofocus
            />
          </fieldset>
          <fieldset>
            <input
              placeholder="Your Email Address"
              type="email"
              tabindex="2"
              required
            />
          </fieldset>
          <fieldset>
            <input
              placeholder="Phone Number"
              type="tel"
              tabindex="3"
              required
            />
          </fieldset>

          <fieldset>
            <textarea placeholder="Address(optional)" tabindex="5"></textarea>
          </fieldset>
          <fieldset>
            <h3>Nominee Details</h3>
            <fieldset>
              <input
                placeholder="Nominee Name"
                type="text"
                tabindex="3"
                required
              />
            </fieldset>
          </fieldset>
          <fieldset>
            <h3>Bike Details</h3>
            <fieldset>
              <input
                placeholder="Registration Number"
                type="text"
                tabindex="3"
                required
              />
              <input
                placeholder="Chassis Number (optional)"
                type="text"
                tabindex="3"
              />
              <input
                placeholder="Engine Number (optional)"
                type="text"
                tabindex="3"
              />
            </fieldset>
            <button
              name="submit"
              type="submit"
              id="contact-submit"
              data-submit="...Sending"
            >
              Submit
            </button>
          </fieldset>
        </form>
      </div>
    </div>
  );
}

export default UserDetail;
