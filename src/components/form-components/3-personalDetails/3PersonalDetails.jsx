import React from 'react'
import UserDetailForm from './UserDetail'

export default function PersonalDetails() {
    return (
        <div>
            <h4>Great Choice! A few more details to go.</h4>
            <UserDetailForm/>
        </div>
    )
}
