import React from "react";
import "./infotable.style.scss";

const InfoTable = ({ state }) => {
  return (
    <div className="receipt">
      <h2 className="merchant-name">Your Entered Details</h2>
      <hr />
      <div className="grid-data">
        {!state.registrationCity ? (
          <></>
        ) : (
          <>
            <div className="item-title" data-id="mark's-diner-avocado-blt">
              Your city:
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.registrationCity}
            </div>
          </>
        )}

        <div className="item-title" data-id="mark's-diner-avocado-blt">
          Basic Details
        </div>
        
        {!state.bikeName ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-avocado-blt"
            >
              2. Bike Name:
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.bikeName}
            </div>
          </>
        )}
        {!state.registrationYear ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-avocado-blt"
            >
              3. Year of Registration :
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.registrationYear}
            </div>
          </>
        )}
        {!state.sumInsured ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-avocado-blt"
            >
              3. Bike Sum Insured:
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.sumInsured}
            </div>
          </>
        )}
        <div className="item-title" data-id="mark's-diner-cinnamon-macchiato">
          Choose Plan
        </div>
        {!state.planType ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-cinnamon-macchiato"
            >
              1.Plan Type
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.planType}
            </div>
          </>
        )}
        {!state.planAmount ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-cinnamon-macchiato"
            >
              1.Plan Amount
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.planAmount}
            </div>
          </>
        )}
        {!state.durationYear ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-cinnamon-macchiato"
            >
              2.Duration Policy
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.durationYear} years
            </div>
          </>
        )}
        {!state.extraCoverages ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-cinnamon-macchiato"
            >
              3.Extra coverages
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.extraCoverages}
            </div>
          </>
        )}
        <div className="item-title" data-id="mark's-diner-donation">
          Owner Details
        </div>
        {!state.name ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              1. full name
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.name}
            </div>
          </>
        )}
        {!state.proffesion ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              2. Your Proffesion
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.proffesion}
            </div>
          </>
        )}
        {!state.pinCode ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              3. pincode
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.pinCode}
            </div>
          </>
        )}
        {!state.email ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              4. email-address
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.email}
            </div>
          </>
        )}
        {!state.mobileNumber ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              5. phone no.
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.mobileNumber}
            </div>
          </>
        )}
        {!state.address ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              6. address
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.address}
            </div>
          </>
        )}
        <div className="item-title" data-id="mark's-diner-donation">
          Nominee Details
        </div>
        {!state.nomineeName ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              1. nominee name
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.nomineeName}
            </div>
          </>
        )}
        {!state.relation ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              2. relationship with nominee
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.relation}
            </div>
          </>
        )}
        <div className="item-title" data-id="mark's-diner-donation">
          Bike Details
        </div>
        {!state.registrationNumber ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              1. registration no
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.registrationNumber}
            </div>
          </>
        )}
        {!state.registrationDate ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              1. registration Date
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.registrationDate}
            </div>
          </>
        )}
        {!state.chassisNumber ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              2. chassis no
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.chassisNumber}
            </div>
          </>
        )}
        {!state.engineNumber ? (
          <></>
        ) : (
          <>
            <div
              className="item-description subtle"
              data-id="mark's-diner-donation"
            >
              3. engine no
            </div>
            <div
              className="item-part subtle"
              data-id="mark's-diner-avocado-blt"
            >
              {state.engineNumber}
            </div>
          </>
        )}
      </div>
      <div className="grid-data calculations">
        <div className="calc-title">Basic Plan Amount</div>
        <div className="calc-amount">$14.78</div>
        <div className="calc-title">Extra Coverage Charges</div>
        <div className="calc-amount">$0.83</div>
        <div className="calc-title total">Grand Total</div>
        <div className="calc-amount total">$15.61</div>
      </div>
      <div className="footer subtle">Thanks ! Now proceed to Payment</div>
    </div>
  );
};

export default InfoTable;
