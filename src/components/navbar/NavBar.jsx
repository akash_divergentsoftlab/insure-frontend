import React from "react";
import "./navbar.scss";

export default function NavBar() {
  return (
    <div>
      <nav
        style={{ backgroundColor: "#3f51b5" }}
        className="mb-4 navbar navbar-expand-lg "
      >
        <a className="navbar-brand bgColoR" href="/" style={{ color: "white" }}>
          Insure
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent-3"
          aria-controls="navbarSupportedContent-3"
          aria-expanded="false"
          aria-label="Toggle navigation"
          href="/#"
        >
          <span className="navbar-toggler-icon" style={{ color: "white" }}>
            ☰
          </span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent-3">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item dropdown active">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDropdownMenuLink-3"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                href="/#"
                style={{ color: "white" }}
              >
                Products
              </a>
              <div
                className="dropdown-menu dropdown-menu-right dropdown-unique"
                aria-labelledby="navbarDropdownMenuLink-3"
              >
                <a className="dropdown-item" href="/#">
                  Motor
                </a>
                <a className="dropdown-item" href="/#">
                  Health
                </a>
                <a className="dropdown-item" href="/#">
                  Other
                </a>
              </div>
            </li>
          </ul>
          <ul className="navbar-nav ml-auto nav-flex-icons">
            <li className="nav-item">
              <a
                className="nav-link waves-effect waves-light"
                style={{ color: "white" }}
                href="/#"
              >
                <i className="fa fa-twitter"></i>
              </a>
            </li>
            <li className="nav-item">
              <a
                className="nav-link waves-effect waves-light"
                style={{ color: "white" }}
                href="/#"
              >
                <i className="fa fa-google-plus"></i>
              </a>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDropdownMenuLink"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                href="/#"
                style={{ color: "white" }}
              >
                <i className="fa fa-user"></i>
              </a>
              <div
                className="dropdown-menu dropdown-menu-right dropdown-unique"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <a className="dropdown-item" href="/#">
                  My Profile
                </a>
                <a className="dropdown-item" href="/#">
                  My Insurance
                </a>
                <a className="dropdown-item" href="/#">
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}
