import React from "react";
import PropTypes from "prop-types";

import { FormField } from "./FormField";

export function Textfield({ label, name, ...props }) {
  return (
    <FormField label={label} name={name}>
      <input type="text" {...props} />
    </FormField>
  );
}

Textfield.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};
