import React, { useState, useEffect } from "react";
import "./UserInfoStep.scss";
import { makeStyles } from "@material-ui/core/styles";
import { useContactFormState } from "../ContactFormContext";
import { Form, Textfield, FormField } from "../../ui";
import axios from "axios";
import { URl } from "../../../BASICURL";
import inf from "indian-number-format";

const useStyles = makeStyles((theme) => ({
  button: {
    display: "block",
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

export function UserInfoStep() {
  const classes = useStyles();
  const {
    state: {
      planType,
      planAmount,
      registrationCity,
      bikeName,
      sumInsured,
      registrationYear,
    },
    dispatch,
  } = useContactFormState();
  const [brands, setBrands] = useState([]);
  const [brandName, setBrandName] = useState("");
  const [city, setCity] = useState("");
  const [model, setModel] = useState("");
  const [year, setYear] = useState("");
  const [plans, setPlans] = useState([]);
  const [sumInsureds, setSumInsureds] = useState("");
  const [open, setOpen] = React.useState(false);
  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(Number(event.target.value) || "");
    // setPlans(event.target.value);
    console.log(plans);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.table(e);

    const url = "http://localhost:9000/api/bikeplan/list";
    const bodyData = {
      brandName: brandName,
      city: city,
      model: model,
      year: year,
      sumInsured: sumInsured,
    };

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(bodyData),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setPlans([...res]);
      });
  };

  const getBrands = () => {
    axios.get(`${URl}/bikebrands/`).then((response) => {
      //   console.log(response);
      const myBrand = response.data;
      setBrands(myBrand);
      console.log(myBrand);
    });
  };

  useEffect(() => {
    getBrands();
  }, []);

  return (
    <>
      <h1>Enter Your Basic details: </h1>

      <Form onSubmit={handleSubmit}>
        <select
          name="bikebrands"
          id="bikebrands"
          onChange={(e) => {
            dispatch({ type: "BIKE_NAME_CHANGE", payload: e.target.value });
            setModel(e.target.value);
          }}
          value={bikeName}
          handleChange={handleChange}
        >
          {brands.map((item) => (
            <>
              <optgroup label={item.brandName}>
                <option>Select Bike model...</option>
                {item.bikes.map((items) => (
                  <option>{items.model}</option>
                ))}
              </optgroup>
            </>
          ))}
        </select>
        <br />
        <br />
        <br />

        <Textfield
          label="Enter city"
          name="city"
          onChange={(e) => {
            dispatch({ type: "CITY_CHANGE", payload: e.target.value });
            setCity(e.target.value);
          }}
          value={registrationCity}
        />

        <Textfield
          label="Enter year of registration(YYYY): "
          name="registrationYear"
          onChange={(e) => {
            dispatch({
              type: "REGISTRATION_YEAR_CHANGE",
              payload: e.target.value,
            });
            setYear(e.target.value);
          }}
          value={registrationYear}
        />
        <Textfield
          label="Enter Sum Inssured(optional)"
          name="sumInsured"
          onChange={(e) => {
            dispatch({
              type: "SUMINSURRED_CHANGE",
              payload: e.target.value,
            });
            setSumInsureds(e.target.value);
          }}
          value={sumInsured}
        />
        {/* bootstrap modal starts here
         */}
        <button
          type="submit"
          class="btn btn-primary"
          data-toggle="modal"
          data-target="#exampleModalCenter"
        >
          View Plans
          {/* the viewPlan button is here  */}
        </button>

        <div
          class="modal fade"
          id="exampleModalCenter"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header1">
                <h5 class="modal-title" id="exampleModalLongTitle">
                  Please select Any plans:
                </h5>
                <p style={{ color: "red" }}>
                  Please Check all the boxes to accept the policy.
                </p>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                {/* modal starts here  */}

                <div class="grid">
                  {plans.slice(0, 1).map((item) => (
                    <>
                      <label
                        key={item.planId}
                        class="card"
                        style={{ width: "220px" }}
                      >
                        <input
                          checked={planType === item.planType}
                          id={item.planType}
                          name={item.planType}
                          value={item.planType}
                          onChange={(e) => {
                            dispatch({
                              type: "PLANTYPE_CHANGE",
                              payload: e.target.value,
                            });
                          }}
                          name="plan"
                          class="radio"
                          type="checkbox"
                        />
                        <span class="plan-details">
                          <span class="plan-type">{item.planType}</span>

                          <span class="plan-cost">
                            ₹{inf.format(item.planAmount)}/year
                            <input
                              type="checkbox"
                              style={{ marginLeft: "10px" }}
                              name={item.planAmount}
                              value={item.planAmount}
                              onChange={(e) => {
                                dispatch({
                                  type: "PLAN_AMOUNT_CHANGE",
                                  payload: e.target.value,
                                });
                              }}
                            />
                          </span>
                          <span>
                            ₹{inf.format(item.sumInsured)}
                            <input
                              type="checkbox"
                              style={{ marginLeft: "109px" }}
                              name={item.sumInsured}
                              value={item.sumInsured}
                              onChange={(e) => {
                                dispatch({
                                  type: "SUMINSURRED_CHANGE",
                                  payload: e.target.value,
                                });
                              }}
                            />
                          </span>
                          <span>{item.description}</span>
                        </span>
                      </label>
                    </>
                  ))}
                </div>

                {/* {plans.map((item) => (
                  <>
                    <FormField
                      label={item.planType}
                      name={item.planType}
                      key={item.planId}
                    >
                      <input
                        type="radio"
                        id={item.planType}
                        name={item.planType}
                        value={item.planType}
                        checked={planType === item.planType}
                        onChange={(e) => {
                          dispatch({
                            type: "PLANTYPE_CHANGE",
                            payload: e.target.value,
                          });
                        }}
                      />
                    </FormField>
                  </>
                ))} */}

                {/* <FormField
                  label="Comprehensive Cover"
                  name="Comprehensive Cover"
                >
                  {plans.slice(0, 1).map((item) => (
                    <label>₹{item.planAmount}</label>
                  ))}
                  <input
                    type="radio"
                    id="Comprehensive Cover"
                    name="planType"
                    value="Comprehensive Cover"
                    checked={planType === "Comprehensive Cover"}
                    onChange={(e) => {
                      dispatch({
                        type: "PLANTYPE_CHANGE",
                        payload: e.target.value,
                      });
                      
                    }}
                  />
                </FormField>
                <FormField label="Third-Party Cover" name="Third-Party Cover">
                  {plans.slice(1, 2).map((item) => (
                    <label>₹{item.planAmount}</label>
                  ))}
                  <input
                    type="radio"
                    id="Third-Party Cover"
                    name="planType"
                    value="Third-Party Cover"
                    checked={planType === "Third-Party Cover"}
                    onChange={(e) =>
                      dispatch({
                        type: "PLANTYPE_CHANGE",
                        payload: e.target.value,
                      })
                    }
                  />
                </FormField> */}
                {/* modal ends here  */}
              </div>
            </div>
          </div>
        </div>

        {/* bootstrap modal ends here  */}
      </Form>
    </>
  );
}

export default UserInfoStep;
