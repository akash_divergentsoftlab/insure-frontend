import React from "react";

import { Form, Textfield } from "../../ui";
import { useContactFormState } from "../ContactFormContext";

export function CategoriesStep() {
  const {
    state: {
      name,
      proffesion,
      pinCode,
      email,
      mobileNumber,
      address,
      nomineeName,
      relation,
      registrationNumber,
      chassisNumber,
      registrationDate,
    },
    dispatch,
  } = useContactFormState();

  return (
    <div className="User-detail-form">
      <h1>Enter user details:</h1>
      <p>
        <em>Enter All the details: </em>
      </p>
      <Form>
        <br />
        <br />
        <hr/>
        <h3>Enter Personal Details: </h3>
        <Textfield
          label="Enter name: "
          name="name"
          onChange={(e) =>
            dispatch({ type: "FULL_NAME_CHANGE", payload: e.target.value })
          }
          value={name}
        />
        <Textfield
          label="Enter Your proffesion: "
          name="proffesion"
          onChange={(e) =>
            dispatch({ type: "PROFFESION_CHANGE", payload: e.target.value })
          }
          value={proffesion}
        />
        <Textfield
          label="Enter pincode"
          name="pinCode"
          onChange={(e) =>
            dispatch({ type: "PINCODE_CHANGE", payload: e.target.value })
          }
          value={pinCode}
        />
        <Textfield
          label="Enter email"
          name="email"
          onChange={(e) =>
            dispatch({ type: "EMAIL_CHANGE", payload: e.target.value })
          }
          value={email}
        />
        <Textfield
          label="Enter mobile Number"
          name="mobileNumber"
          onChange={(e) =>
            dispatch({ type: "MOBILE_NUMBER_CHANGE", payload: e.target.value })
          }
          value={mobileNumber}
        />
        <Textfield
          label="Enter address"
          name="address"
          onChange={(e) =>
            dispatch({ type: "ADDRESS_CHANGE", payload: e.target.value })
          }
          value={address}
        />
        <hr/>
        <h3>Enter Nominee Details:</h3>
        <Textfield
          label="Enter nominee Name"
          name="nomineeName"
          onChange={(e) =>
            dispatch({ type: "NOMINEE_NAME_CHANGE", payload: e.target.value })
          }
          value={nomineeName}
        />

        <label for="relation">Relation with nominee: </label>
        <select
          name="relation"
          onChange={(e) =>
            dispatch({
              type: "NOMINEE_RELATION_CHANGE",
              payload: e.target.value,
            })
          }
          value={relation}
          id="relation"
          style={{ width: "41%", marginLeft: "170px" }}
        >
          <option value="">Select</option>
          <optgroup label="Family Members">
            <option value="son">Son</option>
            <option value="daughter">Daughter</option>
            <option value="father">Father</option>
            <option value="mother">Mother</option>
            <option value="other family member">Other family member</option>
          </optgroup>
          <optgroup label="Non-family">
            <option value="friend">Friend</option>
            <option value="colleague">Colleague</option>
          </optgroup>
          <optgroup label="Others">
            <option value="myself">Myself</option>
            <option value="Other">Other</option>
          </optgroup>
        </select>
          <hr/>
          <h3>Enter Bike Details: </h3>
        <Textfield
          label="Enter registration Number"
          name="registrationNumber"
          onChange={(e) =>
            dispatch({
              type: "REGISTRATION_NUMBER_CHANGE",
              payload: e.target.value,
            })
          }
          value={registrationNumber}
        />
        <Textfield
          label="Enter Date of Registration (YYYY-MM-DD): "
          name="registrationDate"
          onChange={(e) =>
            dispatch({
              type: "REGISTRATION_DATE_CHANGE",
              payload: e.target.value,
            })
          }
          value={registrationDate}
        />
        <Textfield
          label="Enter chassis Number"
          name="chassisNumber"
          onChange={(e) =>
            dispatch({ type: "CHASSIS_NUMBER_CHANGE", payload: e.target.value })
          }
          value={chassisNumber}
        />
      </Form>
    </div>
  );
}
