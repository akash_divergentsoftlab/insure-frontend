import React, { useState, useEffect } from "react";
import "./UserInfoStep.css";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import { useContactFormState } from "../ContactFormContext";
import { Form, Textfield, FormField } from "../../ui";
import axios from "axios";
import { URl } from "../../../BASICURL";

const useStyles = makeStyles((theme) => ({
  button: {
    display: "block",
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

export function UserInfoStep() {
  const classes = useStyles();
  const {
    state: {
      planType,
      planAmount,
      registrationCity,
      bikeName,
      sumInsured,
      registrationYear,
    },
    dispatch,
  } = useContactFormState();
  const [brands, setBrands] = useState([]);
  const [brandName, setBrandName] = useState("");
  const [city, setCity] = useState("");
  const [model, setModel] = useState("");
  const [year, setYear] = useState("");
  const [plans, setPlans] = useState([]);
  const [sumInsureds, setSumInsureds] = useState("");
  const [open, setOpen] = React.useState(false);
  const [age, setAge] = React.useState("");

  const handleChange = (event) => {
    setAge(Number(event.target.value) || "");
    // setPlans(event.target.value);
    console.log(plans);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.table(e);

    const url = "http://localhost:9000/api/bikeplan/list";
    const bodyData = {
      brandName: brandName,
      city: city,
      model: model,
      year: year,
      sumInsured: sumInsured,
    };

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(bodyData),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setPlans([...res]);
      });
  };

  const getBrands = () => {
    axios.get(`${URl}/bikebrands/`).then((response) => {
      //   console.log(response);
      const myBrand = response.data;
      setBrands(myBrand);
      console.log(myBrand);
    });
  };

  useEffect(() => {
    getBrands();
  }, []);

  return (
    <>
      <h1>Enter Your Basic details: </h1>

      <Form onSubmit={handleSubmit}>
        <select
          name="bikebrands"
          id="bikebrands"
          onChange={(e) => {
            dispatch({ type: "BIKE_NAME_CHANGE", payload: e.target.value });
            setModel(e.target.value);
          }}
          value={bikeName}
          handleChange={handleChange}
        >
          {brands.map((item) => (
            <>
              <optgroup label={item.brandName}>
                <option>Select Bike model...</option>
                {item.bikes.map((items) => (
                  <option>{items.model}</option>
                ))}
              </optgroup>
            </>
          ))}
        </select>
        <br />
        <br />
        <br />

        <Textfield
          label="Enter city"
          name="city"
          onChange={(e) => {
            dispatch({ type: "CITY_CHANGE", payload: e.target.value });
            setCity(e.target.value);
          }}
          value={registrationCity}
        />

        <Textfield
          label="Enter year of registration(YYYY): "
          name="registrationYear"
          onChange={(e) => {
            dispatch({
              type: "REGISTRATION_YEAR_CHANGE",
              payload: e.target.value,
            });
            setYear(e.target.value);
          }}
          value={registrationYear}
        />
        <Textfield
          label="Enter Sum Inssured(optional)"
          name="sumInsured"
          onChange={(e) => {
            dispatch({
              type: "SUMINSURRED_CHANGE",
              payload: e.target.value,
            });
            setSumInsureds(e.target.value);
          }}
          value={sumInsured}
        />
        {/* bootstrap modal starts here
         */}
        <button
          type="submit"
          class="btn btn-primary"
          data-toggle="modal"
          data-target="#exampleModalCenter"
        >
          View Plans
          {/* the viewPlan button is here  */}
        </button>

        <div
          class="modal fade"
          id="exampleModalCenter"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                  Please select Any plans:
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                {/* modal starts here  */}

                {plans.map((item) => (
                  <div class="card" key={item.planId}>
                    <div class="header">
                      <p>{item.planType}</p>
                    </div>
                    <div class="container">
                      <p>{item.description}</p>
                      <p>Plan amount- ₹{item.planAmount}</p>
                      <p>Sum Insured ₹{item.sumInsured}</p>
                    </div>
                  </div>
                ))}

                {/* plan selector  */}

                <Button onClick={handleClickOpen}>Open select dialog</Button>
                <Dialog open={open} onClose={handleClose}>
                  <DialogTitle>Fill the form</DialogTitle>
                  <DialogContent>
                    <form className={classes.container}>
                      <FormControl className={classes.formControl}>
                        <InputLabel htmlFor="demo-dialog-native">
                          Plan Name:
                        </InputLabel>
                        <Select
                          native
                          // value={age}
                          onChange={handleChange}
                          input={<Input id="demo-dialog-native" />}
                        >
                          <option aria-label="None" value="" />
                          {plans.map((plan) => (
                            <option key={plan.planId} value={plan.planType}>
                              {plan.planType}
                            </option>
                          ))}
                        </Select>
                      </FormControl>
                      <FormControl className={classes.formControl}>
                        <InputLabel id="demo-dialog-select-label">
                          Plan Amount:
                        </InputLabel>
                        <Select
                          labelId="demo-dialog-select-label"
                          id="demo-dialog-select"
                          // value={age}
                          onChange={handleChange}
                          input={<Input />}
                        >
                          <MenuItem value="">
                            <em>None</em>
                          </MenuItem>
                          {plans.map((plan) => (
                          <MenuItem key={plan.planId} value={plan.planAmount}>{plan.planAmount}</MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </form>
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleClose} color="primary">
                      Cancel
                    </Button>
                    <Button onClick={handleClose} color="primary">
                      Ok
                    </Button>
                  </DialogActions>
                </Dialog>

                {/* plan selector ends here  */}

                {/* {plans.map((item) => (
                  <>
                    <FormField
                      label={item.planType}
                      name={item.planType}
                      key={item.planId}
                    >
                      <input
                        type="radio"
                        id={item.planType}
                        name={item.planType}
                        value={item.planType}
                        checked={planType === item.planType}
                        onChange={(e) => {
                          dispatch({
                            type: "PLANTYPE_CHANGE",
                            payload: e.target.value,
                          });
                        }}
                      />
                    </FormField>
                  </>
                ))} */}

                {/* <FormField
                  label="Comprehensive Cover"
                  name="Comprehensive Cover"
                >
                  {plans.slice(0, 1).map((item) => (
                    <label>₹{item.planAmount}</label>
                  ))}
                  <input
                    type="radio"
                    id="Comprehensive Cover"
                    name="planType"
                    value="Comprehensive Cover"
                    checked={planType === "Comprehensive Cover"}
                    onChange={(e) => {
                      dispatch({
                        type: "PLANTYPE_CHANGE",
                        payload: e.target.value,
                      });
                      
                    }}
                  />
                </FormField>
                <FormField label="Third-Party Cover" name="Third-Party Cover">
                  {plans.slice(1, 2).map((item) => (
                    <label>₹{item.planAmount}</label>
                  ))}
                  <input
                    type="radio"
                    id="Third-Party Cover"
                    name="planType"
                    value="Third-Party Cover"
                    checked={planType === "Third-Party Cover"}
                    onChange={(e) =>
                      dispatch({
                        type: "PLANTYPE_CHANGE",
                        payload: e.target.value,
                      })
                    }
                  />
                </FormField> */}
                {/* modal ends here  */}
              </div>
            </div>
          </div>
        </div>

        {/* bootstrap modal ends here  */}
      </Form>
    </>
  );
}

export default UserInfoStep;
