import React, { useState } from "react";

const ConfirmationPage = ({ state }) => {
  const [address, setAddress] = useState("");
  const [bikeName, setBikeName] = useState("");
  const [chassisNumber, setChassisNumber] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNumber, SetMobileNumber] = useState("");
  const [name, setName] = useState("");
  const [nomineeName, setNomineeName] = useState("");
  const [numberOfYears, setNumberOfYears] = useState("");
  const [pinCode, setPinCode] = useState("");
  const [planType, setPlanType] = useState("");
  const [proffesion, setProffesion] = useState("");
  const [registrationCity, setRegistrationCity] = useState("");
  const [registrationDate, setRegistrationDate] = useState("");
  const [registrationNumber, setRegistrationNumber] = useState("");
  const [relation, setRelation] = useState("");
  const [sumInsured, setSumInsured] = useState("");
  const [planInsured, setPlanInsured] = useState([]);

  const handlechange = (event) => {
    setPlanInsured(event.target.value);
    // console.log(planInsured);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.table(e);

    const url = "http://localhost:9000/api/bikeplan/getBikeInsurance";
    const bodyData = {
      address: address,
      bikeName: bikeName,
      chassisNumber: chassisNumber,
      email: email,
      mobileNumber: mobileNumber,
      name: name,
      nomineeName: nomineeName,
      numberOfYears: numberOfYears,
      pinCode: pinCode,
      planType: planType,
      proffesion: proffesion,
      registrationCity: registrationCity,
      registrationDate: registrationDate,
      registrationNumber: registrationNumber,
      relation: relation,
      sumInsured: sumInsured,
    };

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(bodyData),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setPlanInsured([...res]);
      });
  };

  return (
    <div class="container">
      <div class="row">
        <h4 class="text-danger">Check your details before moving on !</h4>

        <table class="table table-bordered success">
          <thead>
            <tr>
              <th>Full Name</th>
              <td>{state.name}</td>
            </tr>
            <tr>
              <th>Email</th>
              <td>{state.email}</td>
            </tr>
            <tr>
              <th>Mobile Number </th>
              <td>{state.mobileNumber}</td>
            </tr>
            <tr>
              <th>Address: </th>
              <td>{state.address}</td>
            </tr>
            <tr>
              <th>Pincode: </th>
              <td>{state.pinCode}</td>
            </tr>
            <tr>
              <th>City of Registration: </th>
              <td>{state.registrationCity}</td>
            </tr>
            <tr>
              <th>profession</th>
              <td>{state.proffesion}</td>
            </tr>
            <tr>
              <th>Bike Name: </th>
              <td>{state.bikeName}</td>
            </tr>
            <tr>
              <th>Registration Number: </th>
              <td>{state.registrationNumber}</td>
            </tr>
            <tr>
              <th>Chassis Number: </th>
              <td>{state.chassisNumber}</td>
            </tr>
            <tr>
              <th>Registration Year</th>
              <td>{state.registrationYear}</td>
            </tr>
            <tr>
              <th>Type of Plan </th>
              <td>{state.planType}</td>
            </tr>
            <tr>
              <th>Duration of Policy: </th>
              <td>{state.durationYeara}</td>
            </tr>
            <tr>
              <th>Sum Insured Amount </th>
              <td>{state.sumInsured}</td>
            </tr>
            <tr>
              <th>Amount of Plan: </th>
              <td>{state.planAmount}</td>
            </tr>
            <tr>
              <th>Nominee Name</th>
              <td>{state.nomineeName}</td>
            </tr>
            <tr>
              <th>Relation with Nominee</th>
              <td>{state.relation}</td>
            </tr>
            <tr>
              <th>Date of Registration: </th>
              <td>{state.registrationDate}</td>
            </tr>

            <tr>
              <th colspan="1"></th>
              <td>
                <input
                  type="hidden"
                  name="hidden_id"
                  id="hidden_id"
                  value="<?php echo $value->id; ?>"
                />
                <input
                  type="hidden"
                  name="folder_name"
                  id="folder_name"
                  value="<?php echo $value->email; ?>"
                />
                <a
                  href="<?php echo base_url()?>module_c/approval_manager/service_provider_display"
                  class="btn btn-warning"
                >
                  Back
                </a>
                <input
                  type="button"
                  id="approve_btn"
                  class="btn btn-info"
                  name="approve_btn"
                  value="Approve"
                />
              </td>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  );
};

export default ConfirmationPage;
