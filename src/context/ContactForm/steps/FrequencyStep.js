import React from "react";

import { Form, FormField, Textfield } from "../../ui";
import { useContactFormState } from "../ContactFormContext";

export function FrequencyStep() {
  const {
    state: { planType, durationYear, extraCoverages },
    dispatch,
  } = useContactFormState();

  const availableCategories = [
    "personal accident cover - RS.220",
    "breakdown assistance - RS.40",
    "zero depreciation cover - RS.213",
  ];

  const handleChange = (category) => {
    const isSelected = extraCoverages.includes(category);
    let payload = extraCoverages;

    if (isSelected) {
      payload = extraCoverages.filter((c) => c !== category);
    } else {
      payload = [...extraCoverages, category];
    }

    dispatch({
      type: "EXTRA_COVERAGE_CHANGE",
      payload,
    });
  };

  return (
    <>
      <h1>Would you like to add extra coverages?</h1>
      <br />
      <br />
      <br />
      <hr />

      <Form>
        <hr />
        <label for="durationYear">Enter years of plan:</label>
        <select
          onChange={(e) =>
            dispatch({ type: "DURATION_YEAR_CHANGE", payload: e.target.value })
          }
          value={durationYear}
          name="durationYear"
          id="durationYear"
          style={{ width: "41%", marginLeft: "170px" }}
        >
          <option value="1">1 year</option>
          <option value="2">2 year</option>
          <option value="3">3 year</option>
          <option value="4">4 year</option>
          <option value="5">5 year</option>
        </select>
        <hr />
        <h5>Extra coverages: </h5>
        {availableCategories.map((category) => (
          <FormField key={category} label={category} name={category}>
            <input
              type="checkbox"
              name={category}
              id={category}
              value={category}
              onChange={() => handleChange(category)}
              checked={extraCoverages.includes(category)}
            />
          </FormField>
        ))}
      </Form>
    </>
  );
}
