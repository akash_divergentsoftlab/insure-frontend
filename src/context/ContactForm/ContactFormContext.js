import React, { createContext, useReducer, useContext } from "react";

function formReducer(state, action) {
  switch (action.type) {
    case "CITY_CHANGE":
      return { ...state, registrationCity: action.payload };
    case "BIKE_NAME_CHANGE":
      return { ...state, bikeName: action.payload };
    case "REGISTRATION_YEAR_CHANGE":
      return { ...state, registrationYear: action.payload };
    case "SUMINSURRED_CHANGE":
      return { ...state, sumInsured: action.payload };
    case "PLANTYPE_CHANGE":
      return { ...state, planType: action.payload };
    case "DURATION_YEAR_CHANGE":
      return { ...state, durationYear: action.payload };
    case "EXTRA_COVERAGE_CHANGE":
      return { ...state, extraCoverages: action.payload };
    case "FULL_NAME_CHANGE":
      return { ...state, name: action.payload };
    case "PINCODE_CHANGE":
      return { ...state, pinCode: action.payload };
    case "EMAIL_CHANGE":
      return { ...state, email: action.payload };
    case "MOBILE_NUMBER_CHANGE":
      return { ...state, mobileNumber: action.payload };
    case "ADDRESS_CHANGE":
      return { ...state, address: action.payload };
    case "NOMINEE_NAME_CHANGE":
      return { ...state, nomineeName: action.payload };
    case "NOMINEE_RELATION_CHANGE":
      return { ...state, relation: action.payload };
    case "REGISTRATION_NUMBER_CHANGE":
      return { ...state, registrationNumber: action.payload };
    case "CHASSIS_NUMBER_CHANGE":
      return { ...state, chassisNumber: action.payload };
    case "PROFFESION_CHANGE":
      return { ...state, proffesion: action.payload };
    case "PLAN_AMOUNT_CHANGE":
      return { ...state, planAmount: action.payload };
    case "REGISTRATION_DATE_CHANGE":
      return { ...state, registrationDate: action.payload };
    case "PLAN_ARRAY_MERGE_ORIGINAL_ARRAY":
      return { ...state, planArrayMerge: action.payload };
    case "SUBMIT":
      return { ...state, isSubmitLoading: true };
    case "SUBMISSION_RECEIVED":
      return { ...state, isSubmitLoading: false, isSubmissionReceived: true };

    default:
      throw new Error();
  }
}

const ContactFormContext = createContext();

const initialState = {
  address: "",
  registrationYear: "",
  bikeName: "",
  chassisNumber: "",
  email: "",
  mobileNumber: "",
  name: "",
  nomineeName: "",
  durationYear: "",
  pinCode: "",
  planAmount: "",
  planType: "",
  proffesion: "",
  registrationCity: "",
  registrationDate: "",
  registrationNumber: "",
  relation: "",
  sumInsured: "",

  extraCoverages: [],

  isSubmitLoading: false,
  isSubmissionReceived: false,
};

export const ContactFormProvider = function ({ children }) {
  const [state, dispatch] = useReducer(formReducer, initialState);

  return (
    <ContactFormContext.Provider value={{ state, dispatch }}>
      {children}
    </ContactFormContext.Provider>
  );
};

export function useContactFormState() {
  const context = useContext(ContactFormContext);

  if (context === undefined) {
    throw new Error(
      "useContactFormState must be used within a ContactFormProvider"
    );
  }

  return context;
}
