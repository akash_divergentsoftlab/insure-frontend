import React, { Component } from "react";
import "./getinsurance.styles.scss";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Container from "@material-ui/core/Container";
import { ValidatorForm } from "react-material-ui-form-validator";
import rexter from "rexter";

import Basics from "../components/form-components/1-basics/1Basics";
import ChoosePlan from "../components/form-components/2-choosePlan/2ChoosePlan";
import PersonalDetails from "../components/form-components/3-personalDetails/3PersonalDetails";
import Payment from "../components/form-components/4-Payment/4Payment";
import InfoTable from "../components/information-table/InfoTable";

const sitekey = "e76d04b5-85fd-4c27-b158-d521e16e1382";
const styles = {
  root: {
    flexGrow: 1,
    width: "100%",
  },
  menuButton: {
    marginRight: 8,
  },
  title: {
    flexGrow: 1,
  },
  appBarBottom: {
    top: "auto",
    bottom: 0,
  },
  grow: {
    flexGrow: 1,
  },
  paper: {
    paddingBottom: 90,
  },
  text: {
    padding: 2,
  },
  backButton: {
    marginRight: 8,
  },
  instructions: {
    marginTop: 8,
    marginBottom: 8,
  },
};
class GetInsurance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      // eslint-disable-next-line
      data: {},
      disabled: false,
      captchaValid: false,
      submitted: false,
    };
  }
  getSteps = () => {
    return [
      "1. Basics Details",
      "2. Choose plan",
      "3. Personal Details",
      "4. Payment",
    ];
  };

  onChange = (event) => {
    const { data } = this.state;
    data[event.target.name] = event.target.value;
    this.setState({ data });
  };

  submit = () => {
    this.form.submit();
  };

  handleSubmit = () => {
    this.setState({ submitted: true }, () => {
      setTimeout(() => this.setState({ submitted: false }), 5000);
    });
  };

  prevStep = () => {
    let { step } = this.state;
    if (step > 1) {
      step--;
    }
    this.setState({ step });
  };

  nextStep = () => {
    this.form.isFormValid(false).then((isValid) => {
      if (isValid) {
        let { step } = this.state;
        if (step < 4) {
          step++;
        }
        this.setState({ step });
      }
    });
  };

  handleVerificationSuccess = async (response) => {
    // note - if it wreen't for CORS this woulld work
    var bodyFormData = new FormData();
    bodyFormData.set("secret", sitekey);
    bodyFormData.set("response", response);
    try {
      const { data } = await rexter.post(
        "https://hcaptcha.com/siteverify",
        bodyFormData,
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          crossdomain: true,
        }
      );
      console.log("server response:", data);
      //setValidation(data);
      this.setState({ captchaValid: data });
    } catch (err) {
      console.log("CANNOT SUBMIT:", response, err);
    }
  };

  validatorListener = (result) => {
    this.setState({ disabled: !result });
  };

  renderStep = () => {
    // eslint-disable-next-line
    const { step, data } = this.state;
    let content = null;
    switch (step) {
      case 1:
        content = (
          <div>
            <Basics />
          </div>
        );
        break;
      case 2:
        content = <ChoosePlan />;
        break;
      case 3:
        content = (
          <div>
            <PersonalDetails />
          </div>
        );
        break;
      case 4:
        content = (
          <div>
            <Payment />
          </div>
        );
        break;
      default:
        content = <div>Error</div>;
        break;
    }
    return content;
  };
  render() {
    const { classes } = this.props;
    const { step, disabled, submitted, captchaValid } = this.state;
    //const [activeStep, setActiveStep] = this.props;
    const steps = this.getSteps();
    return (
      <div className={classes.root}>
        <Paper square className={classes.paper}>
          <div className={classes.root}>
            {/* the row starts here  */}
            <div className="row">
              {/* column 1 */}
              <div className="column">
                <Stepper activeStep={step - 1} alternativeLabel>
                  {steps.map((label) => (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </div>
              {/* column2 */}
              <div className="column">
                <InfoTable />
              </div>
            </div>
            {/* row ends here  */}
            <ValidatorForm
              ref={(r) => {
                this.form = r;
              }}
              onSubmit={this.handleSubmit}
              instantValidate
            >
              <Container>{this.renderStep()}</Container>
              <AppBar
                position="fixed"
                color="primary"
                className={classes.appBarBottom}
              >
                <Toolbar>
                  <Button
                    onClick={this.prevStep}
                    style={{ marginRight: "16px" }}
                    disabled={step === 1}
                  >
                    previous
                  </Button>
                  <div className={classes.grow} />
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={step < 4 ? this.nextStep : this.submit}
                    disabled={step < 4 ? disabled || submitted : !captchaValid}
                  >
                    {(submitted && "Your form is submitted!") ||
                      (step < 4 ? "Next" : "Submit")}
                  </Button>
                </Toolbar>
              </AppBar>
            </ValidatorForm>
          </div>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(GetInsurance);
