import React, { useState } from "react";
import "./example.css";

export default function Example() {
  const [brandName, setBrandName] = useState("");
  const [city, setCity] = useState("");
  const [model, setModel] = useState("");
  const [year, setYear] = useState("");
  const [plans, setPlans] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.table(e);

    const url = "http://localhost:9000/api/bikeplan/list";
    const bodyData = {
      brandName: brandName,
      city: city,
      model: model,
      year: year,
    };

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(bodyData),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setPlans([...res]);
      });
  };

  return (
    <div>
      <h1>This is API testing</h1>
      <form onSubmit={handleSubmit}>
        Brand name:{" "}
        <input type="text" onChange={(e) => setBrandName(e.target.value)} />
        <br />
        City: <input type="text" onChange={(e) => setCity(e.target.value)} />
        <br />
        Model: <input type="text" onChange={(e) => setModel(e.target.value)} />
        <br />
        Year: <input type="text" onChange={(e) => setYear(e.target.value)} />
        <br />
        <button type="submit">View Plans</button>
      </form>

      <div class="container1">
        <h1>Select Any Plan: </h1>

        <div class="row">
          {plans.map((item) => (
            <div key={item.planId} class="col-md-4 col-lg-4 col-sm-4">
              <label>
                <input type="radio" name="product" class="card-input-element" />
                <div class="panel panel-default card-input">
                  <div class="panel-heading">{item.planType}</div>
                  <div class="panel-body">
                    <p>{item.description}</p>
                    <p>{item.planAmount}</p>
                    <p>{item.sumInsured}</p>
                    </div>
                </div>
              </label>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
