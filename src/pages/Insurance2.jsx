import React from "react";
import ContactForm from "../context/ContactForm/ContactForm";
import { ContactFormProvider } from "../context/ContactForm/ContactFormContext";

function Insurance2() {
  return (
    <ContactFormProvider>
      <ContactForm />
    </ContactFormProvider>
  );
}

export default Insurance2;
