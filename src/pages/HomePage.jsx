import React from "react";
import SearchBox from "../components/homepage-components/searchbox/SearchBox";

export default function HomePage() {
  return (
    <div className="Homepage">
      <SearchBox />
    </div>
  );
}
