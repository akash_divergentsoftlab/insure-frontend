import React from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Paper from "@material-ui/core/Paper";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Container from "@material-ui/core/Container";

import {
  Form,
  ValidatorForm,
  TextValidator
} from "react-material-ui-form-validator";
import "./styles.css";

const styles = {
  root: {
    flexGrow: 1,
    width: "100%"
  },
  menuButton: {
    marginRight: 8
  },
  title: {
    flexGrow: 1
  },
  appBarBottom: {
    top: "auto",
    bottom: 0
  },
  grow: {
    flexGrow: 1
  },
  paper: {
    paddingBottom: 50
  },
  text: {
    padding: 2
  },
  backButton: {
    marginRight: 8
  },
  instructions: {
    marginTop: 8,
    marginBottom: 8
  }
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      data: {
        email1: "",
        email2: "",
        email3: ""
      },
      disabled: false,
      submitted: false
    };
  }

  onChange = event => {
    const { data } = this.state;
    data[event.target.name] = event.target.value;
    this.setState({ data });
  };

  submit = () => {
    this.form.submit();
  };

  handleSubmit = () => {
    this.setState({ submitted: true }, () => {
      setTimeout(() => this.setState({ submitted: false }), 5000);
    });
  };

  prevStep = () => {
    let { step } = this.state;
    if (step > 1) {
      step--;
    }
    this.setState({ step });
  };

  nextStep = () => {
    this.form.isFormValid(false).then(isValid => {
      if (isValid) {
        let { step } = this.state;
        if (step < 3) {
          step++;
        }
        this.setState({ step });
      }
    });
  };

  validatorListener = result => {
    this.setState({ disabled: !result });
  };

  renderStep = () => {
    const { step, data } = this.state;
    let content = null;
    switch (step) {
      case 1:
        content = (
          <TextValidator
            key={1}
            name="email1"
            label="email 1"
            validators={["required", "isEmail"]}
            errorMessages={["required field", "invalid email"]}
            value={data.email1}
            onChange={this.onChange}
            validatorListener={this.validatorListener}
          />
        );
        break;
      case 2:
        content = (
          <TextValidator
            key={2}
            name="email2"
            label="email 2"
            validators={["required", "isEmail"]}
            errorMessages={["required field", "invalid email"]}
            value={data.email2}
            onChange={this.onChange}
            validatorListener={this.validatorListener}
          />
        );
        break;
      case 3:
        content = (
          <TextValidator
            key={3}
            name="email3"
            label="email 3"
            validators={["required", "isEmail"]}
            errorMessages={["required field", "invalid email"]}
            value={data.email3}
            onChange={this.onChange}
            validatorListener={this.validatorListener}
          />
        );
        break;
    }
    return content;
  };

  render() {
    const { classes } = this.props;
    const { step, disabled, submitted } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              News
            </Typography>
            <Button color="inherit">Login</Button>
          </Toolbar>
        </AppBar>
        <Stepper activeStep={activeStep} alternativeLabel>
          {steps.map(label => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
            </Step>
          ))}
        </Stepper>
        <Paper square className={classes.paper}>
          <ValidatorForm
            ref={r => {
              this.form = r;
            }}
            onSubmit={this.handleSubmit}
            instantValidate
          >
            <Container>{this.renderStep()}</Container>
            )}
            <AppBar
              position="fixed"
              color="primary"
              className={classes.appBarBottom}
            >
              <Toolbar>
                <Button
                  onClick={this.prevStep}
                  style={{ marginRight: "16px" }}
                  disabled={step === 1}
                >
                  previous
                </Button>
                <div className={classes.grow} />
                <Button
                  color="primary"
                  variant="contained"
                  onClick={step < 3 ? this.nextStep : this.submit}
                  disabled={disabled || submitted}
                >
                  {(submitted && "Your form is submitted!") ||
                    (step < 3 ? "Next" : "Submit")}
                </Button>
              </Toolbar>
            </AppBar>
          </ValidatorForm>
        </Paper>
      </div>
    );
  }
}
export default withStyles(styles)(App);
